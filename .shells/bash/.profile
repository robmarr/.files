export HISTCONTROL=ignorespace

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob;

# Append to the Bash history file, rather than overwriting it
shopt -s histappend;

# Autocorrect typos in path names when using `cd`
shopt -s cdspell;

# Enable some Bash 4 features when possible:
# * `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
# * Recursive globbing, e.g. `echo **/*.txt`
for option in autocd globstar; do
	shopt -s "$option" 2> /dev/null;
done;

# load share/bash_completion
if [ -f /usr/local/share/bash-completion/bash_completion ]; then
	source /usr/local/share/bash-completion/bash_completion;
fi

# load completion files from /usr/local/etc/bash_completion.d
if [ -d /usr/local/etc/bash_completion.d/ ]; then
	for file in /usr/local/etc/bash_completion.d/*; do
		[ -r "$file" ] && [ -f "$file" ] && source "$file";
	done;
	unset file;
fi

# load /etc/bash_completion
if [ -f /etc/bash_completion ]; then
	source /etc/bash_completion;
fi

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;

# Add gpg & gpg agent
export GPG_TTY=$(tty)
[ -n "$(command -v gpg-agent)" ] && gpg-agent --daemon
	