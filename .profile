if [ ! -t 1 ] ; then  exit 0; fi

cd $HOME/.shells/

export OS="$(uname -s)"
folders=". $OS"

# check what flavour of shell is running
if [ -n "$BASH_VERSION" ]; then
	folders="$folders bash"
fi

# On linux also look for distro specific files
if [ $OS == 'Linux' ]; then
	source /etc/os-release
	folders="$folders $OS/$ID"
fi

# find all relevant dotfiles and load them
files="$(find $folders -maxdepth 1 -type f -name ".*")"
for file in $files; do
	[ -r "$HOME/.shells/$file" ] && [ -f "$HOME/.shells/$file" ] && source "$HOME/.shells/$file"
done;

# cleanup
unset file;
unset files;
unset folders;

cd -  > /dev/null 2>&1;
